<?php
/**
 * Created by PhpStorm.
 * User: A.Syl20 Brénam SOH
 * Date: 25.05.17
 * Time: 09h29
 */
?>
<?php
session_start();
require ('inclusions/lesfonctions.php');
$_SESSION['local']='fr';
$_GET['id']=get_session('user_id');
require ('inclusions/init.php');
require('initialisation/local.php');
require('traductions/menu.php');
require ('inclusions/constantes.php');
require ('filtres/filtreAdmin.php');

        $req=$db->query("SELECT * FROM users ");
        $nombre_total_user=$req->rowCount();
        if ($nombre_total_user>=1){

            $nombre_user_par_page=12;

            $nombre_max_doitre_gauche=4;

            $last_page=ceil($nombre_total_user/$nombre_user_par_page);

            if (isset($_GET['page'])&& is_numeric($_GET['page'])){
                $num_page=$_GET['page'];
            }else{
                $num_page=1;
            }

            if ($num_page<1){
                $num_page=1;
            }else if($num_page>$last_page){
                $num_page=$last_page;
            }

            $limit=' LIMIT '.($num_page-1)*$nombre_user_par_page.','.$nombre_user_par_page;

            $q=$db->query("SELECT * FROM users WHERE active='1' ORDER BY id  ");
            $users=$q->fetchAll(PDO::FETCH_OBJ);
            require('vue/administration.vue.php');

        }else{
            set_flash('Aucun utilisateur pour le moment .....','danger');
            redirect('index.php');
        }

?>
